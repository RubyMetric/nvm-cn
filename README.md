<div align="center">

# nvm-cn

</div>

<br>

`nvm-cn` 解决的是 NodeJS 自身的国内下载问题，而 **npm 包** 的国内下载问题，您可阅读最下方手动进行换源，或通过 [RubyMetric/chsrc] 多平台自动测速换源。

<br>

## 维护消息

- `<2023-09-07>` 组织名变更为 **RubyMetric**，请您谅解URL的变化。[变更原因](https://www.yuque.com/ccmywish/blog/from-rubykids-to-rubymetric)

- `<2023-07-20>` Gitee官方追踪的上游仓库被删除，现切换至备用仓库，由我手动更新

- `<2023-05-01>` 现使用Gitee官方管理的`nvm`镜像，每日更新

- `<2022-11-07>` 此仓库最开始只是方便我自己临时使用，跟随 RubyMetric 的其他项目一起公开出来了。但目前我很少使用 NodeJS，对其生态不是很熟悉，若出现问题，希望您帮助。[Woody's voice box 理念](https://www.yuque.com/ccmywish/blog/woody-voice-box)

<br>

## 安装

```bash
# 执行
bash -c "$(curl -fsSL https://gitee.com/RubyMetric/nvm-cn/raw/main/install.sh)"

# 安装完成后执行
source ~/.nvm/nvm.sh

# 此时可以查到版本信息则表示安装成功
nvm -v
```

## 卸载
```bash
bash -c "$(curl -fsSL https://gitee.com/RubyMetric/nvm-cn/raw/main/uninstall.sh)"
```


## 使用

```bash
nvm ls

# 列出所有可安装版本
nvm ls-remote

# 安装某个版本Node
nvm install lts/fermium
nvm install v12.20.1
nvm install v15.5.1

# 切换Node版本
nvm use system
nvm use 14.15    # 不用全部打出版本号

# 更新nvm
nvm-update
```

<br>

## npm 换源

推荐您使用C语言编写的全平台换源工具 [RubyMetric/chsrc]

![chsrc](https://gitee.com/RubyMetric/image/raw/main/chsrc.png)

`chsrc` 将测速挑选最快镜像站，并自动执行下述命令：

```bash
# 查看配置
npm config ls

npm config set registry https://registry.npmmirror.com
```

[RubyMetric]: https://gitee.com/RubyMetric
[RubyMetric/chsrc]: https://gitee.com/RubyMetric/chsrc
